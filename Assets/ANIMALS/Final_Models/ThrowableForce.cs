﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThrowableForce : MonoBehaviour
{
    Vector3 placeToGo;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        gameObject.transform.position = Vector3.MoveTowards(gameObject.transform.position, placeToGo, 10 * Time.deltaTime);

        float remainingDistance = Vector3.Distance(gameObject.transform.position, placeToGo);

        if (remainingDistance < 0.0001)
        {
            Destroy(gameObject);
        }
    }

    public void Destiny(Vector3 point)
    {

    }
}
