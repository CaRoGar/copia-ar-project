﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuIcons : MonoBehaviour
{
    public Sprite On, Off;
    Sprite currentSprite;
    Image startingImage;
    
    public GameObject sceneController;
    MenuControl sceneControllerScript;

    AudioSource clickSound;

    void Start()
    {
        startingImage = gameObject.GetComponent<Image>();
        clickSound = gameObject.GetComponent<AudioSource>();

        sceneControllerScript = sceneController.GetComponent<MenuControl>();
    }

    void Update()
    {
        currentSprite = startingImage.sprite;
    }

    public void SelfPressed()
    {
        if (currentSprite.name == "Music_Volume_On")
        {
            gameObject.GetComponent<Image>().sprite = Off;

            if (this.gameObject.name == "Music_Button")
            {
                sceneControllerScript.SendMessage("isMusicActive", false);

            } else if (this.gameObject.name == "Sound_Button")
            {
                sceneControllerScript.SendMessage("areSoundsActive", false);
            }
        }

        if (currentSprite.name == "Music_Volume_Off")
        {
            gameObject.GetComponent<Image>().sprite = On;

            if (this.gameObject.name == "Music_Button")
            {
                sceneControllerScript.SendMessage("isMusicActive", true);

            } else if (this.gameObject.name == "Sound_Button")
            {
                sceneControllerScript.SendMessage("areSoundsActive", true);
            }
        }

        clickSound.Play();
    }
}
