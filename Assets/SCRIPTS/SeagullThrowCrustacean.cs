﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SeagullThrowCrustacean : MonoBehaviour
{
    public GameObject Crab, Lobster, Squid, Fish;
    public List<GameObject> Throwables = new List<GameObject>();

    public int crabShots, lobsterShots, squidShots, fishShots;
    public List<int> Shots = new List<int>();

    public Image crabIcon, lobsterIcon, squidIcon, fishIcon;
    List<Image> Icons = new List<Image>();

    Button crabButton, lobsterButton, squidButton, fishButton;
    public Button ShootButton;

    public Text crabIcontext, lobsterIconText, squidIconText, fishIconText;

    #region Music & Sounds
    public AudioSource auSource;

    public GameObject Seagull_BGM_Controller;
    public AudioSource Seagull_BGM_Controller_Audiosource;
    #endregion Music & Sounds

    public Camera Cam;

    public GameObject Crosshairs;

    public float currentGameSpeed;

    int crabPointsPerHit, lobsterPointsPerHit, squidPointsPerHit, fishPointsPerHit;
    public List<int> PointsPerShot = new List<int>();

    public Text scoreText;
    public int currentGameScore;

    void Start()
    {
        auSource = gameObject.GetComponent<AudioSource>();

        Seagull_BGM_Controller_Audiosource = Seagull_BGM_Controller.GetComponent<AudioSource>();
        Seagull_BGM_Controller_Audiosource.Play();

        if (MenuControl.isThereMusic == true)
        {
            Seagull_BGM_Controller_Audiosource.volume = 0.500f;

        } else if (MenuControl.isThereMusic == false)
        {
            Seagull_BGM_Controller_Audiosource.volume = 0.000f;
        }

        //
        crabShots = 10;
        lobsterShots = 0;
        squidShots = 0;
        fishShots = 0;

        //

        //
        Throwables.Add(Crab);
        Crab.SetActive(true);

        Shots.Add(crabShots);

        //

        //
        Throwables.Add(Lobster);
        Lobster.SetActive(false);

        Shots.Add(lobsterShots);
        //

        //
        Throwables.Add(Squid);
        Squid.SetActive(false);

        Shots.Add(squidShots);
        //

        //
        Throwables.Add(Fish);
        Fish.SetActive(false);

        Shots.Add(fishShots);
        //

        crabButton = crabIcon.GetComponent<Button>();
        lobsterButton = lobsterIcon.GetComponent<Button>();
        squidButton = squidIcon.GetComponent<Button>();
        fishButton = fishIcon.GetComponent<Button>();

        //

        Icons.Add(crabIcon);
        Icons.Add(lobsterIcon);
        Icons.Add(squidIcon);
        Icons.Add(fishIcon);

        currentGameSpeed = 1.0f;
        Time.timeScale = currentGameSpeed;

        crabPointsPerHit = 1;
        lobsterPointsPerHit = 5;
        squidPointsPerHit = 25;
        fishPointsPerHit = 100;

        PointsPerShot.Add(crabPointsPerHit);
        PointsPerShot.Add(squidPointsPerHit);
        PointsPerShot.Add(lobsterPointsPerHit);
        PointsPerShot.Add(fishPointsPerHit);

        currentGameScore = 0;
        scoreText.text = currentGameScore.ToString();
    }

    void Update()
    {
        if (Shots[0] <= 0)
        {
            crabIcon.color = new Color(255, 255, 255, 100);
            crabButton.interactable = false;

        } else
        {
            crabIcon.color = new Color(255, 255, 255, 255);
            crabButton.interactable = true;
        }

        if (Shots[1] <= 0)
        {
            lobsterIcon.color = new Color(255, 255, 255, 100);
            lobsterButton.interactable = false;

        } else
        {
            lobsterIcon.color = new Color(255, 255, 255, 255);
            lobsterButton.interactable = true;
        }

        if (Shots[2] <= 0)
        {
            squidIcon.color = new Color(255, 255, 255, 100);
            squidButton.interactable = false;

        } else
        {
            squidIcon.color = new Color(255, 255, 255, 255);
            squidButton.interactable = true;
        }

        if (Shots[3] <= 0)
        {
            fishIcon.color = new Color(255, 255, 255, 100);
            fishButton.interactable = false;

        } else
        {
            fishIcon.color = new Color(255, 255, 255, 255);
            fishButton.interactable = true;
        }

        //We use this ray to check if there are any drops on the floor
        Ray pickupRay = Cam.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
        //Ray pickupRay = Cam.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(pickupRay, out RaycastHit hit))
        {
            //If there are, then...
            if (hit.collider.gameObject.CompareTag("LootItems"))
            {
                if (hit.collider.gameObject.name == "(LOOT) Prefab_Crab(Clone)")
                {
                    int addRounds = Shots[0] + 1;
                    Shots[0] = addRounds;

                    crabIcontext.text = Shots[0].ToString();

                } else if (hit.collider.gameObject.name == "(LOOT) Prefab_Lobster(Clone)")
                {
                    int addRounds = Shots[1] + 1;
                    Shots[1] = addRounds;

                    lobsterIconText.text = Shots[1].ToString();

                } else if (hit.collider.gameObject.name == "(LOOT) Prefab_Squid(Clone)")
                {
                    int addRounds = Shots[2] + 1;
                    Shots[2] = addRounds;

                    squidIconText.text = Shots[2].ToString();

                } else if (hit.collider.gameObject.name == "(LOOT) Prefab_Tuna(Clone)")
                {
                    int addRounds = Shots[3] + 1;
                    Shots[3] = addRounds;

                    fishIconText.text = Shots[3].ToString();
                }

                Destroy(hit.collider.gameObject);
            }
        }


    }

    public void Shoot()
    {
        for (int i = 0; i < Throwables.Count; i++)
        {
            if (Throwables[i].gameObject.activeSelf)
            {
                //We get the current amount of shots in the weapon that is currently being used
                int currentIndexOfThrownObject = Throwables.IndexOf(Throwables[i]);

                //We create a new int based on that previous amount and substract 1 from it, so that we can therefore update the value properly.
                int currentShotsOfCurrentWeapon = Shots[currentIndexOfThrownObject] - 1;

                Shots[currentIndexOfThrownObject] = currentShotsOfCurrentWeapon;

                auSource.Play();

                Ray ray = Cam.ViewportPointToRay(new Vector3(0.5F, 0.5F, 0));

                if (Physics.Raycast(ray, out RaycastHit hit))
                {
                    if (hit.collider.gameObject.CompareTag("Car"))
                    {
                        hit.collider.gameObject.SendMessage("DropLoot");
                        Destroy(hit.collider.gameObject);

                        //Based on our current weapon, we calculate how much we are getting for each shot that we hit.
                        int pointsObtained = PointsPerShot[currentIndexOfThrownObject];

                        //We update the score text
                        currentGameScore += pointsObtained;
                        scoreText.text = currentGameScore.ToString();
                    }
                }
            }
        }
    }

    public void ChangeWeapon(int weaponId)
    {
        for (int i = 0; i < Throwables.Count; i++)
        {
            if (i == weaponId)
            {
                Throwables[i].gameObject.SetActive(true);

            } else
            {
                Throwables[i].gameObject.SetActive(false);
            }
        }

        for (int i = 0; i < Icons.Count; i++)
        {
            if (i == weaponId)
            {
               Crosshairs.GetComponent<Image>().sprite = Icons[i].sprite;
            }
        }
    }

    public void PauseGame()
    {
        if (Time.timeScale == 1.0f)
        {
            Time.timeScale = 0.0f;

        } else if (Time.timeScale == 0.0f)
        {
            Time.timeScale = 1.0f;
        }
    }

    public void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
