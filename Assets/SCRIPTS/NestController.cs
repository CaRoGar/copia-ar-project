﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NestController : MonoBehaviour
{
    public GameObject gameOverScreen;
    BoxCollider boxCol;

    public string HighScoreKey;

    public Text gameOverScoreText, gameOverHighScoreText, currentScore;

    void Start()
    {
        gameOverScreen.SetActive(false);
        boxCol = gameObject.GetComponent<BoxCollider>();
        boxCol.enabled = true;

        gameOverHighScoreText.text = (PlayerPrefs.GetInt(HighScoreKey, 0)).ToString();
    }

    void HideUnhideGameOverScreen()
    {
        gameOverScreen.SetActive(true);
        gameOverScoreText.text = currentScore.text;

        if (int.Parse(gameOverScoreText.text) > int.Parse(gameOverHighScoreText.text))
        {
            PlayerPrefs.SetInt(HighScoreKey, int.Parse(gameOverScoreText.text));
            PlayerPrefs.Save();

            gameOverHighScoreText.text = (PlayerPrefs.GetInt(HighScoreKey, 0)).ToString();

        } else if (int.Parse(gameOverScoreText.text) < int.Parse(gameOverHighScoreText.text))
        {
            gameOverHighScoreText.text = (PlayerPrefs.GetInt(HighScoreKey, 0)).ToString();
        }

        Time.timeScale = 0.0f;
    }
}
