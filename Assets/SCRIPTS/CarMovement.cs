﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarMovement : MonoBehaviour
{
    public Vector3 startingPosition;
    public Vector3 Target, LastTarget;
    bool firstDestinationReached, lastDestinationReached;

    public GameObject nestGameObject;

    void Start()
    {
        firstDestinationReached = false;
        lastDestinationReached = false;

        gameObject.transform.LookAt(Target);

        nestGameObject = GameObject.FindGameObjectWithTag("Nest");

        LastTarget = nestGameObject.transform.position;
    }

    void Update()
    {
        if (firstDestinationReached == false && lastDestinationReached == false)
        {
            gameObject.transform.position = Vector3.MoveTowards(gameObject.transform.position, Target, 0.01f * Time.deltaTime);
        }

        if (firstDestinationReached == true && lastDestinationReached == false)
        {
            gameObject.transform.position = Vector3.MoveTowards(gameObject.transform.position, nestGameObject.transform.position, 0.01f * Time.deltaTime);
        }

        if (Vector3.Distance(gameObject.transform.position, Target) <= 0.01f && firstDestinationReached == false)
        {
            firstDestinationReached = true;
            gameObject.transform.LookAt(nestGameObject.transform.position);
        }

        if (Vector3.Distance(gameObject.transform.position, nestGameObject.transform.position) <= 0.01f && firstDestinationReached == true)
        {
            nestGameObject.gameObject.SendMessage("HideUnhideGameOverScreen");
        }
    }

    void IntermediatePointAssignation(Vector3 destination)
    {
        Target = destination;
    }
}
