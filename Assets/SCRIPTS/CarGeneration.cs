﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarGeneration : MonoBehaviour
{
    public GameObject[] Cars;
    public GameObject[] spawnPoints;
    public GameObject[] intermediatePoints;

    public GameObject Ciudad;
    
    void Start()
    {
        Ciudad = GameObject.FindGameObjectWithTag("City");
        InvokeRepeating("CarSpawn", 3.0f, 2.0f);
    }

    public void CarSpawn()
    {
        int randomCarInt = Random.Range(0, Cars.Length);
        GameObject selectedCar = Cars[randomCarInt];

        int randomSpawnPosInt = Random.Range(0, spawnPoints.Length);
        Vector3 selectedSpawnPos = spawnPoints[randomSpawnPosInt].transform.position;

        GameObject SpawnedCar;
        SpawnedCar = Instantiate(selectedCar, selectedSpawnPos, new Quaternion(0, 0, 0, 0));
        SpawnedCar.transform.parent = Ciudad.transform;

        switch (randomSpawnPosInt)
        {
            case 3:
                SpawnedCar.SendMessage("IntermediatePointAssignation", intermediatePoints[3].transform.position);
                break;

            case 2:
                SpawnedCar.SendMessage("IntermediatePointAssignation", intermediatePoints[2].transform.position);
                break;

            case 1:
                SpawnedCar.SendMessage("IntermediatePointAssignation", intermediatePoints[1].transform.position);
                break;

            case 0:
                SpawnedCar.SendMessage("IntermediatePointAssignation", intermediatePoints[0].transform.position);
                break;
        }
    }
}
