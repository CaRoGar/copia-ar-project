using System.Collections;
using System.Collections.Generic;
using UnityEngine;
 
public class CarDeathController : MonoBehaviour
{
    public GameObject CrabPrefab, LobsterPrefab, SquidPrefab, FishPrefab;

    public GameObject Ciudad;
    Vector3 spawnPos;

    void Start()
	{
        Ciudad = GameObject.FindGameObjectWithTag("City");
	}

    private void Update()
    {

    }

    void DropLoot()
    {
        int Randomizer = Random.Range(10, 100);

        if (Randomizer > 50)
        {
            GameObject Crab;
            Crab = Instantiate(CrabPrefab, new Vector3(gameObject.transform.position.x, gameObject.transform.position.y, gameObject.transform.position.z), Quaternion.identity);
            Crab.transform.parent = Ciudad.transform;

        } else if (Randomizer <= 50 && Randomizer > 30)
        {
            GameObject Lobster;
            Lobster = Instantiate(LobsterPrefab, new Vector3(gameObject.transform.position.x, gameObject.transform.position.y, gameObject.transform.position.z), Quaternion.identity);
            Lobster.transform.parent = Ciudad.transform;

        } else if (Randomizer <= 30 && Randomizer > 15)
        {
            GameObject Squid;
            Squid = Instantiate(SquidPrefab, new Vector3(gameObject.transform.position.x, gameObject.transform.position.y, gameObject.transform.position.z), Quaternion.identity);
            Squid.transform.parent = Ciudad.transform;

        } else if (Randomizer <= 15 && Randomizer > 10)
        {
            GameObject Fish;
            Fish = Instantiate(FishPrefab, new Vector3(gameObject.transform.position.x, gameObject.transform.position.y, gameObject.transform.position.z), Quaternion.identity);
            Fish.transform.parent = Ciudad.transform;
        }
    }
}