﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuControl : MonoBehaviour
{
    public GameObject DropDownMenu;

    AudioSource Seagull_BGM;

    public static bool isThereSound, isThereMusic;

    void Start()
    {
        DropDownMenu.SetActive(false);

        Seagull_BGM = gameObject.GetComponent<AudioSource>();
        Seagull_BGM.Play();

        isThereMusic = true;
        isThereSound = true;
    }

    // Update is called once per frame
    void Update()
    {
        Debug.Log(isThereSound);
        Debug.Log(isThereMusic);
    }

    public void StartGame()
    {
        SceneManager.LoadScene("0-Main");
    }

    public void Menu()
    {
        if (DropDownMenu.activeSelf == false)
        {
            DropDownMenu.SetActive(true);

        } else if (DropDownMenu.activeSelf == true)
        {
            DropDownMenu.SetActive(false);
        }
    }

    public void areSoundsActive(bool trueOrFalse)
    {
        Debug.Log("Sounds are on : " + trueOrFalse);

        switch (trueOrFalse)
        {
            case true:
                Debug.Log("Turn on sounds");
                isThereSound = true;
                break;

            case false:
                Debug.Log("Turn off sounds");
                isThereSound = false;
                break;
        }
    }

    public void isMusicActive(bool trueOrFalse)
    {
        Debug.Log("Music is on : " + trueOrFalse);

        switch (trueOrFalse)
        {
            case true:
                Debug.Log("Turn on music");
                Seagull_BGM.volume = 0.500f;
                isThereMusic = true;
                break;

            case false:
                Debug.Log("Turn off music");
                Seagull_BGM.volume = 0.000f;
                isThereMusic = false;
                break;
        }
    }
}
