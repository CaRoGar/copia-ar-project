﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SeagullController : MonoBehaviour
{
    public Camera thisCamera;
    public GameObject Crab, Lobster, Shellfish1, Shellfish2, Shellfish3, Shellfish4, Squid, Fish;

    public GameObject SoundController;
    SoundControllerScript script1;

    [SerializeField]
    public List<GameObject> Throwables = new List<GameObject>();

    void Start()
    {
        thisCamera = gameObject.GetComponent<Camera>();

        Throwables.Add(Crab);
        Throwables.Add(Lobster);
        Throwables.Add(Shellfish1);
        Throwables.Add(Shellfish2);
        Throwables.Add(Shellfish3);
        Throwables.Add(Shellfish4);
        Throwables.Add(Squid);
        Throwables.Add(Fish);

        script1 = SoundController.GetComponent<SoundControllerScript>();

        foreach (GameObject x in Throwables)
        {
            Debug.Log(x.ToString());
        }
    }

    void Update()
    {
        
    }

    void Shoot()
    {
        Ray ray = thisCamera.ViewportPointToRay(new Vector3(0.5F, 0.5F, 0));

        if (Physics.Raycast(ray, out RaycastHit hit))
        {
            //We instantiate the throwables as long as the raycast touches something... doesn't matter if it's a person.
            int randomThrowable = Random.Range(0, Throwables.Count);

            GameObject Animal;
            Animal = Instantiate(Throwables[randomThrowable], gameObject.transform.position, Quaternion.identity);
            Animal.GetComponent<ThrowableForce>().Destiny(hit.point);

            //Depending on the random object that has been thrown, the corresponding sound will be played on the SoundController GameObject.
            switch (randomThrowable)
            {
                case 7:
                    //Crab thrown
                    script1.PlaySoundEffect("crab_Sound");
                    break;
                case 6:
                    //Lobster thrown
                    script1.PlaySoundEffect("lobster_Sound");
                    break;
                case 5:
                    //Shellfish1 thrown
                    script1.PlaySoundEffect("shellfish1_Sound");
                    break;
                case 4:
                    //Shellfish2 thrown
                    script1.PlaySoundEffect("shellfish2_Sound");
                    break;
                case 3:
                    //Shellfish3 thrown
                    script1.PlaySoundEffect("shellfish3_Sound");
                    break;
                case 2:
                    //Shellfish4 thrown
                    script1.PlaySoundEffect("shellfish4_Sound");
                    break;
                case 1:
                    //Squid thrown
                    script1.PlaySoundEffect("squid_Sound");
                    break;
                case 0:
                    //Fish thrown
                    script1.PlaySoundEffect("fish_Sound");
                    break;
            }

            //Now, however, if it really is a person, then depending on which object you have thrown it will deal more or less damage.
            if (hit.transform.tag == "People")
            {
                
            }
        }
    }
}
