﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundControllerScript : MonoBehaviour
{
    public AudioSource soundPlayer;
    //Not needed
    public AudioClip crab_Sound, lobster_Sound, shellfish1_Sound, shellfish2_Sound, shellfish3_Sound, shellfish4_Sound, squid_Sound, fish_Sound;

    public Camera cam;

    public List<AudioClip> ThrowablesSounds = new List<AudioClip>();

    // Start is called before the first frame update
    void Start()
    {
        soundPlayer = gameObject.GetComponent<AudioSource>();

        ThrowablesSounds.Add(crab_Sound);
        ThrowablesSounds.Add(lobster_Sound);
        ThrowablesSounds.Add(shellfish1_Sound);
        ThrowablesSounds.Add(shellfish2_Sound);
        ThrowablesSounds.Add(shellfish3_Sound);
        ThrowablesSounds.Add(shellfish4_Sound);
        ThrowablesSounds.Add(squid_Sound);
        ThrowablesSounds.Add(fish_Sound);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void PlaySoundEffect(string throwableName)
    {
        AudioClip soundToBePlayed = ThrowablesSounds.Find((x) => x.name == throwableName);
        soundPlayer.PlayOneShot(soundToBePlayed);
    }
}
